// Add4 
// adds four numbers (found in RAM[0], …, RAM[3] or R0, …, R3 equivalently) and stores result 
// in RAM[0] (a.k.a. R0). Your program must finish in 30 cycles or fewer.

 @0
 D = M
 @1
 D = D + M
 @2
 D = D + M
 @3
 D = D + M
 @0
 M = D
(End)
@End
0;JMP