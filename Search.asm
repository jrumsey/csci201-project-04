//Search

// given a value to find in R0 and a memory address in R1 (>= 2) 
// that indicates the last position in memory of the values to search through, 
// search through RAM[2]-RAM[n] (where n is the value in R1) for the value R0. 
// If the value is found, set R0 to 1. Otherwise, set R0 to 0. Your program 
// must finish in 200 cycles or fewer.


@Counter
M=1
(Loop)
@Counter
M=M+1
D=M
@1
D=D-M
@NotFound   //breaks look if not found
D;JGT
@Counter
A=M
D=M
@0
D=D-M
@Found      //breaks loop if found
D;JEQ
@Loop
0;JMP

@Loop
D;JNE
(Found)
@0
M=1
@End
0;JMP
(NotFound)
@0
M=0
@End
(End)