// Max4

// finds the largest value in R0-R3 and stores result in R0. 
// Your program must finish in 100 cycles or fewer.

@0
D = M
@1
D = D - M
@Jump1
D;JLT
@Next1
0;JMP

(Jump1)
@1
D = M
@0
M = D

(Next1)
@0
D = M
@2
D = D - M
@Jump2
D;JLT
@Next2
0;JMP

(Jump2)
@2
D = M
@0
M = D

(Next2)
@0
D = M
@3
D = D - M
@Jump3
D;JLT
@Next3
0;JMP

(Jump3)
@3
D = M
@0
M = D

(Next3)

(End)
@End
0;JMP